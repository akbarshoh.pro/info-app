package com.example.infoapp.data

data class ClubData(
    val img :Int,
    val name: Int,
    val text: String
)