package com.example.infoapp.app

import android.app.Application
import com.example.infoapp.domain.AppRepository

class App :Application() {
    override fun onCreate() {
        super.onCreate()
        AppRepository.init()
    }
}