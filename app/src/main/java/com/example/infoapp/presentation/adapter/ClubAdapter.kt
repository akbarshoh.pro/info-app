package com.example.infoapp.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.infoapp.data.ClubData
import com.example.infoapp.databinding.ItemCardBinding

class ClubAdapter : ListAdapter<ClubData, ClubAdapter.VH>(Diff) {
    private var click: ((ClubData) -> Unit)? = null
    private var time = System.currentTimeMillis()

    object Diff : DiffUtil.ItemCallback<ClubData>() {
        override fun areItemsTheSame(oldItem: ClubData, newItem: ClubData): Boolean =
            oldItem == newItem

        override fun areContentsTheSame(oldItem: ClubData, newItem: ClubData): Boolean =
            oldItem == newItem

    }
    inner class VH(private val binding: ItemCardBinding) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener {
                if (System.currentTimeMillis() - time > 1000) {
                    time = System.currentTimeMillis()
                    click?.invoke(getItem(adapterPosition))
                }
            }
        }

        fun bind(int: Int) {
            binding.appCompatImageView.setImageResource(getItem(int).img)
            binding.name.setText(getItem(int).name)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH =
        VH(binding = ItemCardBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: VH, position: Int) =
        holder.bind(position)

    fun setClick(block: (ClubData) -> Unit) {
        click = block
    }

}