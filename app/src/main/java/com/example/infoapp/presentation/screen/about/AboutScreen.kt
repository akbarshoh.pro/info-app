package com.example.infoapp.presentation.screen.about

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.infoapp.R
import com.example.infoapp.databinding.ScreenAboutBinding

class AboutScreen : Fragment(R.layout.screen_about) {
    private lateinit var binding: ScreenAboutBinding
    private val navArgs: AboutScreenArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = ScreenAboutBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.apply {
            img.setImageResource(navArgs.img)
            name.setText(navArgs.name)
            text.text = navArgs.text

            buttonBack.setOnClickListener {
                findNavController().navigateUp()
            }
        }
    }
}