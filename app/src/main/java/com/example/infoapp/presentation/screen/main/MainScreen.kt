package com.example.infoapp.presentation.screen.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.infoapp.R
import com.example.infoapp.data.ClubData
import com.example.infoapp.databinding.ScreenMainBinding
import com.example.infoapp.domain.AppRepository
import com.example.infoapp.presentation.adapter.ClubAdapter

class MainScreen : Fragment(R.layout.screen_main) {
    private var binding: ScreenMainBinding? = null
    private var ls = ArrayList<ClubData>()
    private val adapter: ClubAdapter by lazy { ClubAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = ScreenMainBinding.inflate(inflater, container, false)
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ls = AppRepository.getInstance().getList() as ArrayList<ClubData>

        binding!!.info.setOnClickListener {
            findNavController().navigate(MainScreenDirections.actionMainScreenToInfoScreen())
        }

        binding!!.rv.adapter = adapter
        binding!!.rv.layoutManager = LinearLayoutManager(requireContext())

        adapter.submitList(ls)

        adapter.setClick {
            findNavController().navigate(MainScreenDirections.actionMainScreenToAboutScreen(it.img, it.name, it.text))
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}